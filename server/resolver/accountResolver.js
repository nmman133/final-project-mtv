var db = require('../db/dbhelper');

module.exports = {
    Query: {
        account: (_, args) => db.getAccount(args.account).then((account) => account[0])
    },

    Mutation: {
        addAccount: (_, args, ctx) => {
            var input = args.input;
            if (ctx.right == 0) {
                console.log('No permission');
                return false;
            }
            return db.addAccount(input.username, input.account, input.type, input.balance).then(() => {
                return true;
            }).catch((msg) => {
                console.log(msg);
                return false;
            });
        },

        deleteAccount: (_, args, ctx) => {
            return db.verifyUser(ctx.username, args.account).then((data) => {
                if (data && data.length > 2) {
                    return db.checkBalance(args.account);
                } else {
                    throw "No permission";
                }
            }).then((data) => {
                var value = data[0].balance;
                console.log(value);
                if (value && value > 5000) {
                    throw 'Cannot delete account';
                }
                return db.deleteAccount(args.account);
            }).then(() => {
                return true;
            }).catch((msg) => {
                console.log(msg);
                return false;
            })
        }
    }
}
