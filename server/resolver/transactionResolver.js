var db = require('../db/dbhelper');

module.exports = {
    Query: {
        transactions: (_, args) => db.getTransactionHistory(args.username).then((histories) => histories)
    },

    Mutation: {
        transfer: (_, args, ctx) => {
            var input = args.input;
            return db.verifyUser(ctx.username, input.srcAccount).then((data) => {
                if (data && data.length > 0) {
                    return db.getAccount(input.srcAccount);
                } else {
                    throw "No permission";
                }
            }).then((rows) => {
                var data = rows[0];
                if (data && data.balance >= input.amount && input.amount >= 10000 && input.amount <= 200000000
                        && input.srcAccount != input.desAccount) {
                    return db.transfer(input.srcAccount, input.desAccount, input.amount, input.feer, input.description);
                } else {
                    throw 'Balance is not valid';
                }
            }).then(() => {
                return true;
            }).catch((msg) => {
                console.log(msg);
                return false;
            });
        },

        deposit: (_, args, ctx) => {
            var input = args.input;
            if (ctx.right == 0) {
                return false;
            }
            return db.deposit(input.account, input.amount).then(() => {
                return true;
            }).catch((msg) => {
                console.log(msg);
                return false;
            });
        }
    }
}
