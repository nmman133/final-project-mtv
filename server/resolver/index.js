module.exports = require('lodash').merge(
  require('./userResolver'),
  require('./accountResolver'),
  require('./transactionResolver')
)
