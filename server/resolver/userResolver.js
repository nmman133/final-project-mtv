var db = require('../db/dbhelper');

module.exports = {
    User: {
        accounts: user => db.getAccountsOfUser(user.username).then((accounts) => accounts),
        transactions: user => db.getTransactionHistory(user.username).then((histories) => histories),
        receivers: user => db.getReceiver(user.username).then((receivers) => receivers)
    },

    Query: {
        users: _ => db.getAllUser().then((rows) => rows),
        userByUsername: (_, args) => db.getUserByUsername(args.username).then((user) => user[0]),
        userByAccount: (_, args) => db.getUserByAccount(args.account).then((user) => user[0]),
        mtvUser: (_, args, ctx) => {
            if (ctx.right == 0) return false;
            return db.getMtvUser(args.username).then((user) => user[0]);
        }
    },

    Mutation: {
        addUser: (_, args, ctx) => {
            var input = args.input;
            if (ctx.right == 0) {
                return false;
            }
            return db.addUser(input.username, input.password, input.email, input.phone, input.fullname, input.address).then(() => {
                return true;
            }).catch((msg) => {
                console.log(msg);
                return false;
            });
        },
        addReceiver: (_, args, ctx) => {
            var input = args.input;
            if (ctx.username != input.username) {
                return false;
            }
            return db.addReceiver(input.username, input.account, input.alias).then(() => {
                return true;
            }).catch((msg) => {
                console.log(msg);
                return false;
            });
        },
        deleteReceiver: (_, args, ctx) => {
            if (ctx.username != args.username) {
                return false;
            }
            return db.deleteReceiver(args.username, args.account).then(() => {
                return true;
            }).catch((msg) => {
                console.log(msg);
                return false;
            });
        },
    },
}
