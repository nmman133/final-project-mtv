var express = require('express');
var db = require('../db/dbhelper');
var moment = require('moment');
var tokenHelper = require('../auth/tokenHelper');
var encrypt = require('encrypt-password');
var config = require('../config/config');
var exception = require('../config/exception');
var cookieParser = require('cookie-parser');
var nodemailer =  require('nodemailer'); // khai báo sử dụng module nodemailer

var router = express.Router();
router.use(cookieParser());

router.post('/login', (req, res) => {
    var username = req.body.username;
    var password = req.body.password;

    if (password.length < 6) {
        res.json({
            auth: false,
            error: exception.INVALID_USERNAME_PASSWORD.code,
            msg: exception.INVALID_USERNAME_PASSWORD.msg
        });
        return;
    }

    db.authUser(username, password).then((rows) => {
        var data = rows[0];
        if (!data) {
            throw 'User not found';
        }
        var accessToken = tokenHelper.createAccessToken(data.username, 0);
        var refreshToken = tokenHelper.createRefreshToken(data.username, 0);
        var createTime = moment().unix() + config.refresh_token_expire;
        return db.insertToken(username, refreshToken, createTime, 0).then(() => {
            responseSucess(res, accessToken, refreshToken, data);
        });
    }).catch((msg) => {
        console.log(msg);
        res.json({
            auth: false,
            error: exception.INVALID_USERNAME_PASSWORD.code,
            msg: exception.INVALID_USERNAME_PASSWORD.msg
        });
    });
});

router.post('/mtv/login', (req, res) => {
    var username = req.body.username;
    var password = req.body.password;

    if (password.length < 6) {
        res.json({
            auth: false,
            error: exception.INVALID_USERNAME_PASSWORD.code,
            msg: exception.INVALID_USERNAME_PASSWORD.msg
        });
    }

    db.authMtvUser(username, password).then((rows) => {
        var data = rows[0];
        if (!data) {
            throw 'User not found';
        }
        var accessToken = tokenHelper.createAccessToken(data.username, 1);
        var refreshToken = tokenHelper.createRefreshToken(data.username, 1);
        var createTime = moment().unix() + config.refresh_token_expire;
        return db.insertToken(username, refreshToken, createTime, 0).then(() => {
            responseSucess(res, accessToken, refreshToken, data);
        });
    }).catch((msg) => {
        console.log(msg);
        res.json({
            auth: false,
            error: exception.INVALID_USERNAME_PASSWORD.code,
            msg: exception.INVALID_USERNAME_PASSWORD.msg
        });
    });
});

router.post('/refresh', (req, res) => {
    var token = req.cookies.REFRESH_TOKEN;
    if (!token) {
        responseError(res, exception.NO_TOKEN.code, exception.NO_TOKEN.msg);
        return;
    }
    var right;
    tokenHelper.verifyToken(token, config.refresh_token_secret_key).then((decoded) => {
        right = decoded.right;
        return db.getToken(decoded.username);
    }).then((rows) => {
        var data = rows[0];
        if (!data) {
            throw 'Token not found';
        }
        if (data.token != token) {
            db.blockToken(data.username).then(() => { }).catch((msg) => { });
            responseError(res, exception.ATTACK_DETECTED.code, exception.ATTACK_DETECTED.msg);
            return;
        }
        if (data.block == 1) {
            responseError(res, exception.BLOCKED_TOKEN.code, exception.BLOCKED_TOKEN.msg);
            return;
        }
        if (moment().unix() >= data.expiredTime) {
            responseError(res, exception.EXPIRED_TOKEN.code, exception.EXPIRED_TOKEN.msg);
            return;
        }
        var accessToken = tokenHelper.createAccessToken(data.username, right);
        var refreshToken = tokenHelper.createRefreshToken(data.username, right);
        var expiredTime = moment().unix() + config.refresh_token_expire;
        return db.insertToken(data.username, refreshToken, expiredTime, 0).then(() => {
            responseSucess(res, accessToken, refreshToken, data);
        });
    }).catch((msg) => {
        console.log(msg);
        responseError(res, exception.INVALID_TOKEN.code, exception.INVALID_TOKEN.msg);
    });
});

router.post('/logout', (req, res) => {
    var username = req.body.username;
    res.clearCookie('ACCESS_TOKEN');
    res.clearCookie('REFRESH_TOKEN');
    db.deleteToken(username).then(() => { }).catch((msg) => {
        console.log(msg);
    });
    res.send('done');
});

router.post('/sendMail', (req, res) => {
    var transporter =  nodemailer.createTransport({ // config mail server
        service: 'Gmail',
        auth: {
            user: 'noreply.mtvbanking@gmail.com',
            pass: 'mtv123456'
        }
    });
    var mainOptions = { // thiết lập đối tượng, nội dung gửi mail
        from: 'MTVBANK',
        to: req.body.email,
        subject: ' [MTVBanking] Mã OTP xác nhận giao dịch',
        html: 
            'Xin chào <strong><i>'+ req.body.fullname + '</i></strong>,<br><br>' +
            'Đây là mã OTP để xác nhận giao dịch:<br>' + 
            '<strong>' + req.body.otp + '</strong><br><br>' +
            'Vui lòng không trả lời lại mail này.<br>' +
            'Xin cám ơn,<br> <strong>MTVSupport</strong>'
    }

    transporter.sendMail(mainOptions, function(err, info){
        if (err) {
            console.log(err);
            res.send('done');
        } else {
            console.log('Message sent: ' +  info.response);
            res.send('done');
        }
    });
});

function responseSucess(res, accessToken, refreshToken, data) {
    res.cookie('ACCESS_TOKEN', accessToken, { expire: 360000 + Date.now() });
    res.cookie('REFRESH_TOKEN', refreshToken, { expire: 360000 + Date.now() });
    res.json({
        auth: true,
        user: {
            username: data.username,
            fullname: data.fullname,
            email: data.email
        },
        access_token: accessToken,
        refreshToken: refreshToken
    });
}

function responseError(res, code, msg) {
    res.clearCookie('ACCESS_TOKEN');
    res.clearCookie('REFRESH_TOKEN');
    res.json({
        auth: false,
        error: code,
        msg: msg
    });
}

module.exports = router;
