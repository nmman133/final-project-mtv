var jwt = require('jsonwebtoken');
var config = require('../config/config');

exports.createAccessToken = (username, right) => {
    var data = {
        username: username,
        right: right,
        type: 'access_token'
    };
    return jwt.sign(data, config.access_token_secret_key, { expiresIn: config.access_token_expire });
}

exports.createRefreshToken = (username, right) => {
    var data = {
        username: username,
        right: right,
        type: 'refresh_token'
    };
    return jwt.sign(data, config.refresh_token_secret_key);
}

exports.verifyToken = (token, secretKey) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secretKey, (err, decoded) => {
            if (err) {
                reject('Invalid token');
            } else {
                resolve(decoded);
            }
        });
    });
}
