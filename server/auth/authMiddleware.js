var tokenHelper = require('./tokenHelper');
var config = require('../config/config');
var exception = require('../config/exception');

exports.authMiddleware = (req, res, next) => {
    var token = req.cookies.ACCESS_TOKEN;
    if (token) {
        tokenHelper.verifyToken(token, config.access_token_secret_key).then((decoded) => {
            req.username = decoded.username;
            req.right = decoded.right;
            next();
        }).catch((msg) => {
            console.log(msg);
            res.json({
                auth: false,
                code: exception.INVALID_TOKEN.code,
                msg: exception.INVALID_TOKEN.msg
            });
        })
    } else {
        res.json({
            auth: false,
            code: exception.NO_TOKEN.code,
            msg: exception.NO_TOKEN.msg
        });
    }
}
