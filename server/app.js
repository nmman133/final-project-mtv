var express = require('express');
var authRouter = require('./router/authRouter');
var { authMiddleware } = require('./auth/authMiddleware');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path = require('path');
var config = require('./config/config');
var graphqlHTTP = require('express-graphql');
var { makeExecutableSchema } = require('graphql-tools');
var { importSchema } = require('graphql-import');
var typeDefs = importSchema('./schema/schema.graphql');
var resolvers = require('./resolver/');
var cors = require('cors');

const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});

var graphqlServer = express();

graphqlServer.use(cors({
    origin: true,
    credentials: true
}));
graphqlServer.use(bodyParser.urlencoded({ extended: false }));
graphqlServer.use(bodyParser.json());
graphqlServer.use('/', authRouter);
graphqlServer.use(cookieParser(), authMiddleware);
graphqlServer.use('/graphql', graphqlHTTP(req => {
    return {
        schema: schema,
        graphiql: true,
        context: {
            username: req.username,
            right: req.right,
        }
    }
}));

graphqlServer.listen(process.env.PORT || config.graphql_port, () => {
    console.log(`Server is running on http://localhost:${config.graphql_port}`);
})
