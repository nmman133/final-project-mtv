var mysql = require('mysql');
var moment = require('moment');
var encrypt = require('encrypt-password');
var config = require('../config/config');

const DB_INFO = {
    host     : config.db_host,
    user     : config.db_user,
    password : config.db_pass,
    database : config.db_name,
    port     : config.port,
    multipleStatements: true
};

function createConnection() {
    return mysql.createConnection(DB_INFO);
}

function query(sql) {
    return new Promise((resolve, reject) => {
        var connection = createConnection();
        connection.connect(err => {
            if (err) {
                reject(err.toString());
            }
        });
        connection.query(sql, function (error, results) {
            if (!error) {
                resolve(results);
            } else {
                reject(error.toString());
            }
        });
        connection.end();
    });
}

function transaction(sqls) {
    return new Promise((resolve, reject) => {
        var connection = createConnection();
        connection.connect(err => {
            if (err) {
                reject(err.toString());
            }
        });
        connection.beginTransaction((err_t) => {
            if (err_t) reject(err_t.toString());
            transactionRun(sqls, 0, connection, resolve, reject);
        });
    });
}

function transactionRun(sqls, i, connection, resolve, reject) {
    if (i < sqls.length) {
        connection.query(sqls[i], (error, results) => {
            if (error) {
                reject(error.toString());
                connection.rollback((err) => {});
            } else {
                transactionRun(sqls, i + 1, connection, resolve, reject);
            }
        });
    } else {
        connection.commit((err) => {
            if (err) {
                reject(err.toString());
            }
        });
        resolve();
        connection.end();
    }
}

exports.authUser = (username, password) => {
    password = encrypt(password, config.encrypt_password_config);
    var sql = `select * from user_table where username = '${username}' and password = '${password}'`;
    return query(sql);
}

exports.authMtvUser = (username, password) => {
    password = encrypt(password, config.encrypt_password_config);
    var sql = `select * from mtv_user_table where username = '${username}' and password = '${password}'`;
    return query(sql);
}

exports.getToken = (username) => {
    var sql = `select * from token_table where username = '${username}'`;
    return query(sql);
}

exports.insertToken = (username, token, expiredTime, block) => {
    var sql = `insert into token_table (username, token, expiredTime, block) \
    values('${username}', '${token}', ${expiredTime}, '${block}') \
    on duplicate key update token = '${token}', expiredTime = '${expiredTime}', block = '${block}'`;
    return query(sql);
}

exports.updateToken = (username, token, expiredTime) => {
    var sql = `update token_table set token = '${token}', expiredTime = ${expiredTime} where username = '${username}'`;
    return query(sql);
}

exports.blockToken = (username) => {
    var sql = `update token_table set block = '0' where username = '${username}'`;
    return query(sql);
}

exports.deleteToken = (username) => {
    var sql = `delete from token_table where username = '${username}'`;
    return query(sql);
}

exports.getAllUser = () => {
    var sql = `select * from user_table`;
    return query(sql);
};

exports.getUserByUsername = (username) => {
    var sql = `select * from user_table where username = '${username}'`;
    return query(sql);
};

exports.getUserByAccount = (account) => {
    var sql = `select usr.username, usr.password, usr.email, usr.phone, usr.fullname, usr.address \
    from user_table as usr join account_mapping_table as map on (usr.username = map.username) \
    where map.account = '${account}'`;
    return query(sql);
};

exports.getMtvUser = (username) => {
    var sql = `select username, fullname, role from mtv_user_table where username = '${username}'`;
    return query(sql);
};

exports.addUser = (username, password, fullname, email, phone, address) => {
    password = encrypt(password, config.encrypt_password_config);
    var sql = `insert into user_table (username, password, fullname, email, phone, address)\
    values ('${username}', '${password}', N'${fullname}', '${email}', ' ${phone}', N'${address}')`;
    return query(sql);
};

exports.getAccount = (account) => {
    var sql = `select * from account_table where account = '${account}'`;
    return query(sql);
}

exports.getAccountsOfUser = (username) => {
    var sql = `select acc.account, acc.type, acc.balance from \
    account_table as acc join account_mapping_table as accm on acc.account = accm.account \
    where accm.username = '${username}'`;
    return query(sql);
}

exports.addAccount = (username, account, type, init_balance) => {
    var sql1 = `insert into account_table values ('${account}', ${type}, '${init_balance}');`;
    var sql2 = `insert into account_mapping_table values ('${username}', '${account}');`;
    return transaction([sql1, sql2]);
}

exports.deleteAccount = (account) => {
    var sql1 = `delete from account_mapping_table where account = '${account}'`;
    var sql2 = `delete from account_table where account = '${account}'`;
    return transaction([sql1, sql2]);
}

exports.getReceiver = (username) => {
    var sql = `select * from contact_table where username = '${username}'`;
    return query(sql);
}

exports.addReceiver = (username, account, alias) => {
    var sql = `insert into contact_table values ('${username}', '${account}', '${alias}');`;
    return query(sql);
}

exports.deleteReceiver = (username, account) => {
    var sql = `delete from contact_table where username = '${username}' and account = '${account}'`;
    return query(sql);
}

exports.checkBalance = (account) => {
    var sql = `select balance from account_table where account = '${account}'`;
    return query(sql);
}

exports.deposit = (account, amount) => {
    var description = `Deposit ${amount} to ${account}`;
    var time = moment().format('YYYY-MM-DD HH:mm:ss');
    var sql1 = `update account_table set balance = balance + ${amount} where account = '${account}'`;
    var sql2 = `insert into transaction_history_table (source, destination, type, amount, transactionTime, description) \
    values('mtv_ibank', '${account}', '1', '${amount}', '${time}', '${description}')`;
    return transaction([sql1, sql2]);
}

exports.transfer = (src_account, des_account, amount, feer, description) => {
    var sql1, sql2;
    if (feer == 0) {
        sql1 = `update account_table set balance = balance - ${amount} - ${config.transfer_fee} where account = '${src_account}'`;
        sql2 = `update account_table set balance = balance + ${amount} where account = '${des_account}'`;
    } else {
        sql1 = `update account_table set balance = balance - ${amount} where account = '${src_account}'`;
        sql2 = `update account_table set balance = balance + ${amount} - ${config.transfer_fee} where account = '${des_account}'`;
    }
    var time = moment().format('YYYY-MM-DD HH:mm:ss');
    var sql3 = `insert into transaction_history_table (source, destination, type, amount, transactionTime, description) \
    values('${src_account}', '${des_account}', '0', '${amount}', '${time}', '${description}')`;
    return transaction([sql1, sql2, sql3]);
}

exports.getTransactionHistory = (username) => {
    var sql = `select * from transaction_history_table as th join account_mapping_table as mp on (th.source = mp.account or th.destination = mp.account) where mp.username = '${username}'`;
    return query(sql);
}

exports.verifyUser = (username, account) => {
    var sql = `select * from account_mapping_table where username = '${username}' and account = '${account}'`;
    return query(sql);
}
