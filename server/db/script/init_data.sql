-- mtv_user_table, pass = '000000'
insert into mtv_user_table (username, password, role, fullname)
values ('nmman', '0bDmyFEnrhnlOsHkEW74grJzXmG6VmPZP2NEJlYhvJ4=', 0, N'Nguyễn Minh Mẫn');

insert into mtv_user_table (username, password, role, fullname)
values ('lttt', '0bDmyFEnrhnlOsHkEW74grJzXmG6VmPZP2NEJlYhvJ4=', 0, N'Lê Thị Thiên Trang');

insert into mtv_user_table (username, password, role, fullname)
values ('lav', '0bDmyFEnrhnlOsHkEW74grJzXmG6VmPZP2NEJlYhvJ4=', 0, N'Lý Anh Vũ');

-- user_table, pass = '123456'
insert into user_table (username, password, email, phone, fullname, address)
values ('nmman', 'zgE0pMLc7m2VqGA8qWAcyD89TLtkEMm6CRngPUYzGMc=', 'nmman@outlook.com', '0334704821', N'Nguyễn Minh Mẫn A', N'220 Xô Viết Nghệ Tĩnh');

insert into user_table (username, password, email, phone, fullname, address)
values ('dhnn', 'zgE0pMLc7m2VqGA8qWAcyD89TLtkEMm6CRngPUYzGMc=', 'dhnn@gmail.com', '0336546789', N'Đỗ Hữu Nhật Nguyên', N'123 Đinh Bộ Lĩnh');

insert into user_table (username, password, email, phone, fullname, address)
values ('lttt', 'zgE0pMLc7m2VqGA8qWAcyD89TLtkEMm6CRngPUYzGMc=', 'lttt@outlook.com', '0945335521', N'Lê Thị Thiên Trang', N'240 Xô Viết Nghệ Tĩnh');

insert into user_table (username, password, email, phone, fullname, address)
values ('lav', 'zgE0pMLc7m2VqGA8qWAcyD89TLtkEMm6CRngPUYzGMc=', 'lav@yahoo.com', '01634678943', N'Lý Anh Vũ', N'17 Bà Huyện Thanh Quan');

insert into user_table (username, password, email, phone, fullname, address)
values ('ntnp', 'zgE0pMLc7m2VqGA8qWAcyD89TLtkEMm6CRngPUYzGMc=', 'ntnp@outlook.com', '0335267821', N'Nguyễn Tất Nam Phương', N'156 Điện Biên Phủ');

insert into user_table (username, password, email, phone, fullname, address)
values ('ptpy', 'zgE0pMLc7m2VqGA8qWAcyD89TLtkEMm6CRngPUYzGMc=', 'ptpy@gmail.com', '0364703451', N'Phan Thị Phương Uyên', N'221 Xô Viết Nghệ Tĩnh');

-- account_table
insert into account_table
values ('001234567', 0, '16000000');

insert into account_table
values ('011234567', 1, '5000000');

insert into account_table
values ('101234567', 0, '14000000');

insert into account_table
values ('111234567', 1, '9000000');

insert into account_table
values ('201234567', 0, '20000000');

insert into account_table
values ('211234567', 1, '15000000');

insert into account_table
values ('301234567', 0, '8000000');

insert into account_table
values ('311234567', 1, '10000000');

insert into account_table
values ('401234567', 0, '5000000');

insert into account_table
values ('411234567', 1, '7000000');

insert into account_table
values ('501234567', 0, '12000000');

insert into account_table
values ('511234567', 1, '6000000');

insert into account_table
values ('mtv_ibank', 3, '0');

-- account_mapping_table
insert into account_mapping_table
values ('nmman', '001234567');

insert into account_mapping_table
values ('nmman', '011234567');

insert into account_mapping_table
values ('dhnn', '101234567');

insert into account_mapping_table
values ('dhnn', '111234567');

insert into account_mapping_table
values ('lttt', '201234567');

insert into account_mapping_table
values ('lttt', '211234567');

insert into account_mapping_table
values ('lav', '301234567');

insert into account_mapping_table
values ('lav', '311234567');

insert into account_mapping_table
values ('ntnp', '401234567');

insert into account_mapping_table
values ('ntnp', '411234567');

insert into account_mapping_table
values ('ptpy', '501234567');

insert into account_mapping_table
values ('ptpy', '511234567');

--

insert into contact_table
values ('nmman', '101234567', N'Phi Lý');

insert into contact_table
values ('nmman', '201234567', N'Thiên Chang');
