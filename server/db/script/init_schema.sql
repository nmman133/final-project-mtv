create database mtv_ibank;
use mtv_ibank;

create table mtv_user_table
(
    username char(20),
    password char(50),
    role int,
    fullname nvarchar(50),
    primary key (username)
);

create table user_table
(
    username char(24),
    password char(50),
    fullname nvarchar(50),
    email char(50),
    phone varchar(100),
    address nvarchar(100),
    primary key (username)
);

create table account_table
(
    account char(9) not null,
    type int,
    balance bigint,
    primary key (account)
);

create table account_mapping_table
(
    username char(20) not null,
    account char(9) not null,
    primary key (username, account),
    foreign key (username) references user_table(username),
    foreign key (account) references account_table(account)
);

create table transaction_history_table (
    transactionId int not null auto_increment,
    source char(9),
    destination char(9),
    type int,
    amount bigint,
    transactionTime varchar(50),
    description varchar(280),
    primary key (transactionId),
    foreign key (source) references account_table(account),
    foreign key (destination) references account_table(account)
);

create table contact_table
(
    username char(9),
    account char(9),
    alias nvarchar(50),
    primary key (username, account)
);

create table token_table
(
    username char(20) not null,
    token varchar(200) not null,
    expiredTime int not null,
    block boolean,
    primary key (username)
)
