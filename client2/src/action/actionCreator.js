import * as type from './actionType';
import { callGraphqlApi } from './apiHelper';

export function updateUser(user) {
    return {
        type: type.UPDATE_USER,
        payload: user
    }
}


export function showSuccess(msg) {
    return {
        type: type.SHOW_SUCCESS_MSG_BOX,
        payload: msg
    }
}

export function showError(msg) {
    return {
        type: type.SHOW_ERROR_MSG_BOX,
        payload: msg
    }
}

export function showLoading() {
    return {
        type: type.SHOW_LOADING_BOX,
        payload: null
    }
}

export function hideAllBox() {
    return {
        type: type.HIDE_ALL_BOX,
        payload: null
    }
}

export function fetchUser(username) {
    return dispatch => {
        var query = {
            "query": `{mtvUser(username: "${username}"){username fullname role}}`
        };
        callGraphqlApi(query).then((res) => {
            if (res.status === 200 && !res.data.errors) {
                var data = res.data.data.mtvUser;
                dispatch(updateUser({
                    username: data.username,
                    fullname: data.fullname,
                    role: data.role
                }));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể nạp dữ liệu từ máy chủ. Vui lòng thử lại!'));
        });
    }
}

export function addUserAccount({account, password, name, email, phone, address}) {
    return dispatch => {
        var query = {
            "query": `mutation{addUser(input: {username: "${account}", password: "${password}", fullname: "${name}", email:"${email}", phone: "${phone}", address: "${address}"})}`
        };
        callGraphqlApi(query).then((res) => {
            console.log(res);
            if (res.status === 200 && !res.data.errors && res.data.data.addUser === true) {
                dispatch(showSuccess('Thêm tài khoản người dùng thành công!'));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể thêm tài khoản này. Vui lòng thử lại!'));
        });
    }
}

export function addBankAccount({username, account, balance}) {
    return dispatch => {
        var query = {
            "query": `mutation{addAccount(input: {username: "${username}", account: "${account}", type: 0, balance: "${balance}"})}`
        };
        callGraphqlApi(query).then((res) => {
            console.log(res);
            if (res.status === 200 && !res.data.errors && res.data.data.addAccount === true) {
                dispatch(showSuccess('Thêm tài khoản thanh toán cho người dùng thành công!'));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể thêm tài khoản thanh toán. Vui lòng kiểm tra sự tồn tại của tài khoản người dùng!'));
        });
    }
}

export function deposit({account, amount}) {
    return dispatch => {
        var query = {
            "query": `mutation{deposit(input: {account: "${account}", amount: "${amount}"})}`
        };
        callGraphqlApi(query).then((res) => {
            console.log(res);
            if (res.status === 200 && !res.data.errors && res.data.data.deposit === true) {
                dispatch(showSuccess('Nạp tiền thành công!'));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể nạp tiền cho tài khoản thanh toán này. Vui lòng kiểm tra sự tồn tại của tài khoản!'));
        });
    }
}
