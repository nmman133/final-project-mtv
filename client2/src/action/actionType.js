// Server fetching
export const FETCH_USER = 'FETCH_USER';

// Local update
export const UPDATE_USER = 'UPDATE_USER';

// UI action
export const SHOW_SUCCESS_MSG_BOX = 'SHOW_SUCCESS_MSG_BOX';
export const SHOW_ERROR_MSG_BOX = 'SHOW_ERROR_MSG_BOX';
export const SHOW_LOADING_BOX = 'SHOW_LOADING_BOX';
export const HIDE_ALL_BOX = 'HIDE_ALL_BOX';
