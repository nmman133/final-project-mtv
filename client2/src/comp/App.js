import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Dashboard from './dashboard';
import Error from './error';
import NewBank from './newbank';
import NewAccount from './newaccount';
import Transition from './transition';
import Login from './login';
import MsgBox from './msgBox';

// Component chính, như container chứa các page
class App extends Component {
    render() {
        return (
            <div>
                <BrowserRouter>
                    <Switch>
                        <Route path='/' component={Login} exact />
                        <Route path='/dashboard' component={Dashboard} exact />
                        <Route path='/newaccount' component={NewAccount} exact />
                        <Route path='/newbank' component={NewBank} exact />
                        <Route path='/transition' component={Transition} exact />
                        <Route component={Error} />
                    </Switch>
                        </BrowserRouter>
                        <MsgBox/>
                    </div>
                );
            }
        }

export default App;
