import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { showError, showSuccess, deposit } from '../action/actionCreator';

import Header from './header';

class Deposit extends Component {
    submit(e) {
        e.preventDefault();
        var account = this.state.account;
        var amount = this.state.amount;

        if (account.length !== 9 || isNaN(parseInt(account))) {
            this.props.showError('Số tài khoản phải gồm 9 kí tự số');
            return;
        }

        if (isNaN(parseInt(amount)) || parseInt(amount) > 200000000 || parseInt(amount) < 10000) {
            this.props.showError('Số tiền phải là số nguyên dương tối thiểu 10.000 VND và tối đa 200.000.000 VND');
            return;
        }

        this.props.deposit({
            account,
            amount
        });

        this.props.history.push('./dashboard');
    }

    onAccountChange(e) {
        this.setState({
            account: e.target.value
        });
    }

    onAmountChange(e) {
        this.setState({
            amount: e.target.value
        });
    }

    render() {
        return (
            <div>
                <Header />
                <br></br>
                <div className="container-fluid align-middle col-md-6">
                    <form onSubmit={this.submit.bind(this)}>
                        <div className="form-group">
                            <label >Số tài khoản</label>
                            <span style={{ color: "gray" }}><small>   (Cung cấp username thực)</small></span>
                            <input onChange={e => this.onAccountChange(e)} type="text" className="form-control" id="sotaikhoan" placeholder="Số tài khoản" required></input>
                        </div>
                        <div className="form-group">
                            <label >Số tiền nạp thêm</label>
                            <span style={{ color: "gray" }}><small>   (Tối thiểu 10.000 VND, tối đa 200.000.000 VND)</small></span>
                            <input onChange={e => this.onAmountChange(e)} type="text" className="form-control" id="chutaikhoan" placeholder="Số tiền" required></input>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <Link to={`/dashboard`} activeclassname="active">
                                    <button type="submit" className="btn btn-huy float-right">Hủy</button>
                                </Link>
                            </div>
                            <div className="col-md-6">
                                <button type="submit" className="btn btn-save float-left">Gửi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div >
        );
    }
}

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {
        showError: (msg) => {
            dispatch(showError(msg))
        },
        showSuccess: (msg) => {
            dispatch(showSuccess(msg))
        },
        deposit: (bundle) => {
            dispatch(deposit(bundle))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Deposit);
