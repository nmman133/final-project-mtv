import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { showError, showSuccess, addBankAccount } from '../action/actionCreator';

import Header from './header';

class NewBankAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            balance: 0
        }
    }

    submit(e) {
        e.preventDefault();
        var username = this.state.username;
        var account = this.state.account;
        var balance = this.state.balance;

        if (account.length !== 9 || isNaN(parseInt(account))) {
            this.props.showError('Số tài khoản phải gồm 9 kí tự số');
            return;
        }

        if (isNaN(parseInt(balance)) || parseInt(balance) > 2000000000 || parseInt(balance) < 0) {
            this.props.showError('Số dư khởi tạo phải là số nguyên dương không quá 200.0000.000 VND');
            return;
        }

        this.props.addBankAccount({
            username,
            account,
            balance
        });

        this.props.history.push('./dashboard');
    }

    onAccountNumberChange(e) {
        this.setState({
            account: e.target.value
        });
    }

    onUserAccountChange(e) {
        this.setState({
            username: e.target.value
        });
    }

    onBalanceChange(e) {
        this.setState({
            balance: e.target.value
        });
    }

    render() {
        return (
            <div>
                <Header />
                <br></br>
                <div className="container-fluid align-middle col-md-6">
                    <form onSubmit={this.submit.bind(this)}>
                        <div className="form-group">
                            <label >Tài khoản người dùng</label>
                            <span style={{ color: "gray" }}><small>   (Cung cấp username thực)</small></span>
                            <input onChange={e => this.onUserAccountChange(e)} type="text" className="form-control" id="chutaikhoan" placeholder="Tài khoản" required></input>
                        </div>
                        <div className="form-group">
                            <label >Số tài khoản tạo mới</label>
                            <span style={{ color: "gray" }}><small>   (Dãy số 9 kí tự)</small></span>
                            <input onChange={e => this.onAccountNumberChange(e)} type="text" className="form-control" maxLength="9" id="sotaikhoan" placeholder="Số tài khoản" required></input>
                        </div>
                        <div className="form-group">
                            <label >Số dư khởi tạo</label>
                            <span style={{ color: "gray" }}><small>   (Tối đa 200.000.000 VND, mặc định 0 VND)</small></span>
                            <input onChange={e => this.onBalanceChange(e)} type="text" maxLength="10" className="form-control" id="tengoinho" placeholder="Số dư"></input>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <Link to={`/dashboard`} activeclassname="active">
                                    <button type="submit" className="btn btn-huy float-right">Hủy</button>
                                </Link>
                            </div>
                            <div className="col-md-6">
                                <button type="submit" className="btn btn-save float-left">Mở tài khoản</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div >
        );
    }
}

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {
        showError: (msg) => {
            dispatch(showError(msg))
        },
        showSuccess: (msg) => {
            dispatch(showSuccess(msg))
        },
        addBankAccount: (bundle) => {
            dispatch(addBankAccount(bundle))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewBankAccount);
