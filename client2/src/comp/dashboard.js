import React, { Component } from 'react';
import '../css/dashboard.css';
import newaccount from '../img/NewAccount.png';
import newbank from '../img/NewBank.png';
import money from '../img/Money.png';
import Header from './header';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {
    Card, CardImg, CardBody, CardImgOverlay, Button
} from 'reactstrap';

class Dashboard extends Component {

    render() {
        return (
            <div>
                <Header />
                <div className="row-centered">
                    <Card inverse className="card">
                        <CardImg width="100%" src={newaccount} alt="Card image cap" />
                        <CardImgOverlay>
                            <CardBody>
                                <br></br><br></br><br></br><br></br> <br></br><br></br><br></br>
                                <Link to={`/newaccount`} activeclassname="active">
                                    <Button type="submit" className="btn-card" ><i className="fa fa-user-plus"></i> Tài khoản</Button>
                                </Link>
                            </CardBody>
                        </CardImgOverlay>
                    </Card>

                    <Card inverse className="card">
                        <CardImg width="100%" src={newbank} alt="Card image cap" />
                        <CardImgOverlay>
                            <CardBody>
                                <br></br><br></br><br></br><br></br> <br></br><br></br><br></br>
                                <Link to={`/newbank`} activeclassname="active">
                                    <Button type="submit" className="btn-card" ><i className="fa fa-cc-mastercard"></i> Ngân hàng</Button>
                                </Link>
                            </CardBody>
                        </CardImgOverlay>
                    </Card>

                    <Card inverse className="card">
                        <CardImg width="100%" src={money} alt="Card image cap" />
                        <CardImgOverlay>
                            <CardBody>
                                <br></br><br></br><br></br><br></br> <br></br><br></br><br></br>
                                <Link to={`/transition`} activeclassname="active">
                                    <Button type="submit" className="btn-card" ><i className="fa fa-money"></i> Nạp tiền</Button>
                                </Link>
                            </CardBody>
                        </CardImgOverlay>
                    </Card>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        username: state.userReducer.user.username
    }
}

export default connect(mapStateToProps)(Dashboard);
