import React, { Component } from 'react';
import newaccount from './img/NewAccount.png';
import newbank from './img/NewBank.png';
import money from './img/Money.png';

import {
    Card, CardImg, CardBody, CardImgOverlay, Button
} from 'reactstrap';

class Card extends Component {
    render() {
        return (
            <div>
                <Card inverse className="card">
                    <CardImg width="100%" src={newaccount} alt="Card image cap" />
                    <CardImgOverlay>
                        <CardBody>
                            <br></br><br></br><br></br><br></br> <br></br><br></br><br></br>
                            <Button type="submit" className="btn-card" >New Account</Button>
                        </CardBody>
                    </CardImgOverlay>
                </Card>
            </div>
        );
    }
}

export default Card;
