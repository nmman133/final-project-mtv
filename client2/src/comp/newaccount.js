import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { showError, showSuccess, addUserAccount } from '../action/actionCreator';

import Header from './header';

class NewAccount extends Component {
    submit(e) {
        e.preventDefault();
        var account = this.state.account;
        var password = this.state.password;
        var name = this.state.name;
        var email = this.state.email;
        var phone = this.state.phone;
        var address = this.state.address;

        if (password.length < 6) {
            this.props.showError('Mật khẩu phải có ít nhất 6 kí tự');
            return;
        }

        this.props.addUserAccount({
            account,
            password,
            name,
            email,
            phone,
            address
        });

        this.props.history.push('./dashboard');
    }

    onAccountChange(e) {
        this.setState({
            account: e.target.value
        });
    }

    onPassChange(e) {
        this.setState({
            password: e.target.value
        });
    }

    onNameChange(e) {
        this.setState({
            name: e.target.value
        });
    }

    onEmailChange(e) {
        this.setState({
            email: e.target.value
        });
    }

    onPhoneChange(e) {
        this.setState({
            phone: e.target.value
        });
    }

    onAddressChange(e) {
        this.setState({
            address: e.target.value
        });
    }

    render() {
        return (
            <div>
                <Header />
                <br></br>
                <div className="container-fluid align-middle col-md-6">
                    <form onSubmit={this.submit.bind(this)}>
                        <div className="form-group">
                            <label >Tên tài khoản</label>
                            <span style={{ color: "gray" }}><small>   (Tối thiểu 3 kí tự, tối đa 24 kí tự)</small></span>
                            <input onChange={e => this.onAccountChange(e)} type="text" maxLength="24" className="form-control" id="username" placeholder="Tài khoản" required></input>
                        </div>
                        <div className="form-group">
                            <label >Mật khẩu</label>
                            <span style={{ color: "gray" }}><small>   (Tối thiểu 6 kí tự, tối đa 24 kí tự)</small></span>
                            <input onChange={e => this.onPassChange(e)} type="password" maxLength="24" className="form-control" id="password" placeholder="Mật khẩu" required></input>
                        </div>

                        <div className="form-group">
                            <label >Họ tên</label>
                            <input onChange={e => this.onNameChange(e)} type="text" className="form-control" id="fullname" placeholder="Họ tên" required></input>
                        </div>
                        <div className="form-group">
                            <label >Email</label>
                            <input onChange={e => this.onEmailChange(e)} type="text" className="form-control" id="mail" placeholder="Email" required></input>
                        </div>
                        <div className="form-group">
                            <label >Số điện thoại</label>
                            <input onChange={e => this.onPhoneChange(e)} type="text" className="form-control" id="phone" placeholder="Số điện thoại" required></input>
                        </div>
                        <div className="form-group">
                            <label >Địa chỉ</label>
                            <input onChange={e => this.onAddressChange(e)} type="text" className="form-control" id="address" placeholder="Địa chỉ" required></input>
                        </div>

                        <div className="row">
                            <div className="col-md-6">
                                <Link to={`/dashboard`} activeclassname="active">
                                    <button type="submit" className="btn btn-huy float-right">Hủy</button>
                                </Link>
                            </div>
                            <div className="col-md-6">
                                <button type="submit" className="btn btn-save float-left">Thêm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div >
        );
    }
}

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {
        showError: (msg) => {
            dispatch(showError(msg))
        },
        showSuccess: (msg) => {
            dispatch(showSuccess(msg))
        },
        addUserAccount: (bundle) => {
            dispatch(addUserAccount(bundle))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewAccount);
