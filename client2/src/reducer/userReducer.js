import * as type from '../action/actionType';

const dummy = {
    user: {
        username: '',
        fullname: '',
        role: 0
    }
}

export default function (state = dummy, action) {
    var newState = {...state};
    var payload = action.payload;
    switch (action.type) {
        case type.UPDATE_USER:
            newState.user = payload;
            break;
        default:
            break;
    }
    return newState;
}
