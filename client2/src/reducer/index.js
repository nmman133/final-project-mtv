import { combineReducers } from 'redux';
import userReducer from './userReducer';
import msgBoxReducer from './msgBoxReducer';

const rootReducer = combineReducers({
    userReducer,
    msgBoxReducer
});

export default rootReducer;
