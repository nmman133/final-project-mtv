import React, { Component } from 'react';
import { connect } from 'react-redux';
import Card from './card';

class CardPanel extends Component {

    listCard() {
        var accounts = this.props.accounts;
        var i = 0;
        return accounts.map((acc) => <div className="col-md-4" key={i++}><Card info={acc}/></div>);
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    {this.listCard()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        accounts: state.accountReducer.accounts
    }
}

export default connect(mapStateToProps)(CardPanel);
