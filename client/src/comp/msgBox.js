import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hideAllBox } from '../action/actionCreator';

class MsgBox extends Component {
    render() {
        var modalClass;
        var modalStyle;
        if (this.props.enable === true) {
            modalClass = "modal fade show";
            modalStyle = {
                display: 'block'
            };
        } else {
            modalClass = "modal fade";
            modalStyle = {
                display: 'none'
            };
        }

        const backdropStyle = {
            position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.3)'
        };

        const successMsgBox = (
            <div className={modalClass} role="dialog" aria-hidden="true" style={modalStyle}>
                <div className='backdrop' style={backdropStyle}>
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                Thông báo</div>
                            <div className="modal-body text-center">
                                {this.props.msgBox.msg}
                            </div>
                            <div className="modal-footer row">
                                <div className="col-sm-12">
                                    <div className="text-center">
                                        <button type="button" onClick={this.props.dismiss} className="btn btn-login" data-dismiss="modal">OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        const errorMsgBox = (
            <div className={modalClass} id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style={modalStyle}>
                <div className='backdrop' style={backdropStyle}>
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                Thông báo</div>
                            <div className="modal-body text-center">
                                {this.props.msgBox.msg}
                            </div>
                            <div className="modal-footer row">
                                <div className="col-sm-12">
                                    <div className="text-center">
                                        <button type="button" onClick={this.props.dismiss} className="btn btn-login" data-dismiss="modal">Đồng ý</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        const loadingMsgBox = (
            <div className={modalClass} id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style={modalStyle}>
                <div className='backdrop' style={backdropStyle}>
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                Thông báo
                                </div>
                            <div className="modal-body text-center">
                                Đang tải trang.........
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        switch (this.props.msgBox.type) {
            case 0:
                return successMsgBox;
            case 1:
                return loadingMsgBox;
            case -1:
                return errorMsgBox;
            default:
                return (<div></div>);
        }
    }
}

const mapStateToProps = state => {
    return {
        msgBox: state.msgBoxReducer.msgBox,
        enable: state.msgBoxReducer.msgBox.enable
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dismiss: () => {
            dispatch(hideAllBox());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MsgBox);
