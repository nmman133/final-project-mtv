import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { addContact, showError, fetchUser } from '../action/actionCreator';

import Header from './header';

class Newcontact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            account: '',
            alias: ''
        }
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        this.props.fetchUser(sessionStorage.getItem('user'));
    }

    submit(e) {
        e.preventDefault();
        if (this.state.account.length === 9 && !isNaN(parseInt(this.state.account))) {
            this.props.addContact(sessionStorage.getItem('user'), this.state.account, this.state.alias);
            this.props.history.push('/dashboard');
        } else {
            this.props.showError('Số tài khoản phải là 9 kí tự số');
        }
    }

    onAccountChange(e) {
        this.setState({
            account: e.target.value
        });
    }

    onAliasChange(e) {
        this.setState({
            alias: e.target.value
        });
    }

    render() {
        return (
            <div>
                <Header />
                <br></br>
                <div className="container-fluid align-middle col-md-6">
                    <form onSubmit={this.submit}>
                        <div className="form-group">
                            <label >Số tài khoản</label>
                            <input type="text" data-minlength="9" pattern="[0-9]+" maxLength="9" onChange={e => this.onAccountChange(e)} className="form-control" id="sotaikhoan" placeholder="Số tài khoản" required></input>
                        </div>
                        <div className="form-group">
                            <label >Tên gợi nhớ</label>
                            <input type="text" onChange={e => this.onAliasChange(e)} className="form-control" placeholder="Tên gợi nhớ" required></input>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <Link to={`/dashboard`} activeclassname="active">
                                    <button className="btn btn-huy float-right">Hủy</button>
                                </Link>
                            </div>
                            <div className="col-md-6">
                                <button type="submit" className="btn btn-save float-left">Lưu</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div >
        );
    }
}

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {
        addContact: (username, account, alias) => {
            dispatch(addContact(username, account, alias))
        },
        showError: (msg) => {
            dispatch(showError(msg))
        },
        fetchUser: (username) => {
            dispatch(fetchUser(username))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Newcontact);
