import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteContact } from '../action/actionCreator';

class Receiver extends Component {
    constructor(props) {
        super(props);
        this.deleteReceiver = this.deleteReceiver.bind(this);
    }

    deleteReceiver(e) {
        var idx = e.target.value;
        var receiver = this.props.receivers[idx];
        this.props.deleteReceiver(sessionStorage.getItem('user'), receiver.account);
    }

    listReceiver() {
        var receivers = this.props.receivers;
        var i = 0;
        return receivers.map((receiver) => <tr key={i}>
            <th scope="row">{i}</th>
            <td>{receiver.alias}</td>
            <td>{receiver.account}</td>

            <td className="text-right">
                <Link to={{
                    pathname: '/transfermoney',
                    state: { to: `${receiver.account}` }
                }}>
                    <button className="btn btn-chuyentien">Chuyển tiền</button>
                </Link>
                <button value={i++} onClick={this.deleteReceiver} className="btn btn-xoa">Xóa</button>
            </td>
        </tr>);
    }

    render() {
        return (
            <table className="table table-striped table-hover ">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Số tài khoản</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {this.listReceiver()}
                </tbody>
            </table >
        );
    }
}

const mapStateToProps = state => {
    return {
        receivers: state.receiverReducer.receivers
    }
}

const mapDispatchToProps = dispatch => {
    return {
        deleteReceiver: (username, account) => {
            dispatch(deleteContact(username, account));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Receiver);
