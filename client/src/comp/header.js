import React, { Component } from 'react';
import { connect } from 'react-redux';
import logo from '../img/logo.png';
import axios from 'axios';

class Header extends Component {
    logout(e) {
        const url = 'http://localhost:3300/logout';
        axios.post(url, {
            username: sessionStorage.getItem('user')
        }).then((res) => {
            // Remove session data
            sessionStorage.removeItem('user');
            sessionStorage.removeItem('email');
        }).catch((e) => {});
        this.props.history.replace('/');
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-dark">
                <a href="/">
                    <img src={logo} alt={"logo"} className="d-inline-block align-top "></img>
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle bg-dark no-border text-light" href="/" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {this.props.fullname} ({this.props.username})</a>
                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a onClick={this.logout} className="dropdown-item" href="/">Đăng xuất</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

const mapStateToProps = state => {
    return {
        username: state.userReducer.user.username,
        fullname: state.userReducer.user.fullname
    }
}

export default connect(mapStateToProps)(Header);
