import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Login from './login';
import Dashboard from './dashboard';
import Error from './error';
import Newcontact from './newcontact';
import Transfermoney from './transfermoney';
import MsgBox from './msgBox';

// Component chính, như container chứa các page
class App extends Component {
    render() {
        return (
            <div>
            <BrowserRouter>
                <Switch>
                    <Route path='/' component={Login} exact />
                    <Route path='/dashboard' component={Dashboard} exact />
                    <Route path='/newcontact' component={Newcontact} exact />
                    <Route path='/transfermoney' component={Transfermoney} exact />
                    <Route component={Error} />
                </Switch>
            </BrowserRouter>
            <MsgBox/>
            </div>
        );
    }
}

export default App;
