import React, { Component } from 'react';
import { connect } from 'react-redux';

class History extends Component {

    listHistory() {
        var histories = this.props.history;
        var i = 0;
        return histories.map((history) => <tr key={i}>
            <th scope="row">{i++}</th>
            <td>{history.source}</td>
            <td>{history.destination}</td>
            <td>{history.amount}</td>
            <td>{history.transactionTime}</td>
            <td>{history.description}</td>
        </tr>);
    }

    render() {
        return (
            <table className="table table-striped text-center table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Từ</th>
                        <th scope="col">Đến</th>
                        <th scope="col">Số tiền</th>
                        <th scope="col">Thời gian</th>
                        <th scope="col">Mô tả</th>
                    </tr>
                </thead>
                <tbody>
                    {this.listHistory()}
                </tbody>
            </table>
        );
    }
}

const mapStateToProps = state => {
    return {
        history: state.historyReducer.history
    }
}

export default connect(mapStateToProps)(History);
