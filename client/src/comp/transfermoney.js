import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { fetchUser, fetchAccount, fetchReceiver, fetchHistory, showLoading,
     showSuccess, showError, hideAllBox, addContact } from '../action/actionCreator';
import { connect } from 'react-redux';
import Header from './header';
import { callGraphqlApi } from '../action/apiHelper';
import { sendMail } from '../action/apiHelper';

class Transfermoney extends Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultAccount: '',
            srcAccount: '',
            balance: 0,
            account: {
                success: true,
                msg: '',
                value: ''
            },
            amount: {
                success: true,
                msg: '',
                value: 0
            },
            name: '',
            description: '',
            feer: 0,
            isChecked: false,
            genOTPValue : '',
            codeOTP: '',
            checkOTP : false
        };

        this.handleChecked = this.handleChecked.bind(this);
        this.infoSpan = this.infoSpan.bind(this);
        this.onAccountChange = this.onAccountChange.bind(this);
        this.onAmountChange = this.onAmountChange.bind(this);
        this.onSrcAccountChange = this.onSrcAccountChange.bind(this);
        this.submit = this.submit.bind(this);
        this.onChangeOTP = this.onChangeOTP.bind(this);
        this.sendMailOTP = this.sendMailOTP.bind(this);
    }

    componentDidMount() {
        this.props.fetchUser(sessionStorage.getItem('user'));
        this.props.fetchAccount(sessionStorage.getItem('user'));
        var param = this.props.location.state;
        if (param) {
            this.setState({
                account: {
                    value: param.to
                },
                defaultAccount: param.to
            });
            this.queryUser(param.to);
        }
    }

    handleChecked() {
        this.setState({ isChecked: !this.state.isChecked });
    }

    onSenderFeerChange(e) {
        this.setState({
            feer: 0
        });
    }

    onReceiverFeerChange(e) {
        this.setState({
            feer: 1
        });
    }

    onSrcAccountChange(e) {
        var idx = parseInt(e.target.value);
        var srcAcc = this.props.accounts[idx].account;
        var balance = parseInt(this.props.accounts[idx].balance);
        this.setState({
            srcAccount: srcAcc,
            balance: balance
        });
    }

    listAccount() {
        var accounts = this.props.accounts;
        var i = 0;
        return accounts.map((acc) => <option value={i} key={i++}>{acc.account}</option>);
    }

    queryUser(account) {
        var query = {
            "query": `{userByAccount(account: "${account}"){fullname}}`
        };
        callGraphqlApi(query).then((res) => {
            var data = res.data.data.userByAccount;
            if (res.status === 200 && data) {
                this.setState({
                    name: data.fullname
                });
            } else {
                this.setState({
                    account: {
                        success: false,
                        msg: 'Tài khoản không tồn tại!'
                    },
                    name: ''
                });
            }
        }).catch((e) => {
            console.log(e);
        });
    }

    onAccountChange(e) {
        var account = e.target.value;
        if (account.length === 9 && !isNaN(parseInt(account))) {
            if (this.state.srcAccount !== '' && this.state.srcAccount === account) {
                this.setState({
                    account: {
                        success: false,
                        msg: 'Không thể chuyển cùng tài khoản'
                    }
                });
                return;
            }
            this.queryUser(account);
            this.setState({
                account: {
                    value: account,
                    success: true,
                    msg: ''
                }
            });
        } else {
            this.setState({
                account: {
                    success: false,
                    msg: 'Số tài khoản phải là 9 kí tự số'
                },
                name: ''
            });
        }
    }

    onAmountChange(e) {
        var amount = parseInt(e.target.value);
        if (!isNaN(amount) && amount >= 10000 && amount <= 200000000) {
            this.setState({
                amount: {
                    value: amount,
                    success: true,
                    msg: ''
                }
            });
        } else {
            this.setState({
                amount: {
                    success: false,
                    msg: 'Số tiền chuyển phải là số nguyên ít nhất 10.000, nhiều nhất 2.000.0000'
                }
            });
        }
    }

    submit(e) {
        e.preventDefault();

        var srcAccount = this.state.srcAccount;
        var desAccount = this.state.account.value;
        var amount = this.state.amount.value;
        var description = this.state.description;
        var balance = this.state.balance;

        if (amount > balance) {
            this.props.showError('Không thể chuyển số tiền lớn hơn số dư của tài khoảng hiện tại');
            return;
        }

        // check OTP
        if(this.setState.codeOTP != this.setState.genOTPValue) {
            this.props.showError('Không đúng mã OTP');
            return;
        }

        var query = {
            "query": `mutation{transfer(input: {srcAccount:"${srcAccount}", desAccount: "${desAccount}", amount: "${amount}", feer: ${this.state.feer}, description: "${description}"})}`
        };

        callGraphqlApi(query).then((res) => {
            if (res.status === 200 && res.data && res.data.data && res.data.data.transfer === true) {
                this.props.showSuccess('Chuyển khoản thành công!');
                this.props.history.push('./dashboard');

                var username = sessionStorage.getItem('user');
                this.props.fetchAccount(username);
                this.props.fetchHistory(username);

                if (this.state.isChecked) {
                    this.props.addContact(username, desAccount, this.state.name);
                    this.props.fetchReceiver(username);
                }
            } else {
                this.props.showError('Chuyển khoản không thành công, vui lòng kiểm tra lại thông tin');
            }
        }).catch((e) => {
            console.log(e);
            this.props.showError('Có lỗi trong quá trình xử lý');
        });
    }

    onDescriptionChange(e) {
        this.setState({
            description: e.target.value
        });
    }

    infoSpan(type) {
        if (type === 'account') {
            let style = this.state.account.success ? { color: 'green' } : { color: 'red' };
            return (
                <span style={style}>{this.state.account.msg}</span>
            )
        } else {
            let style = this.state.amount.success ? { color: 'green' } : { color: 'red' };
            return (
                <span style={style}>{this.state.amount.msg}</span>
            )
        }
    }

    sendMailOTP(e) {

        var amount = this.state.amount.value;
        var balance = this.state.balance;

        if (amount > balance) {
            this.props.showError('Không thể chuyển số tiền lớn hơn số dư của tài khoảng hiện tại');
            return;
        }

        e.preventDefault();
        var otp = "";
        var char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
        otp += char.charAt(Math.floor(Math.random() * char.length));

        this.setState.genOTPValue = otp;
        var emailInfo = JSON.parse(sessionStorage.getItem('emailInfo'));
        emailInfo.otp = otp;

        console.log(emailInfo);

        this.setState({ checkOTP: true });
        this.props.showLoading();
        sendMail(emailInfo).then((res) => {
            this.props.showSuccess(`Chúng tôi đã gửi một email xác nhận giao dịch vào tài khoản email ${emailInfo.email}. Hãy kiểm tra hòm thư của bạn!`);
            console.log('Gửi OTP thành công!');
        }).catch((e) => {
            console.log(e);
            this.props.hideAllBox();
        });
    }

    onChangeOTP(e) {
        this.setState.codeOTP = e.target.value;
    }

    render() {
        return (
            <div>
                <Header />
                <br></br>
                <div className="container-fluid align-middle col-md-6">
                    <form onSubmit={this.submit} role="form" data-toggle="validator">
                        <label>Tài khoản nguồn</label>
                        <select defaultValue="" className="form-control" disabled={this.state.checkOTP} onChange={this.onSrcAccountChange} required>
                            <option value="" disabled>Chọn một tài khoản</option>
                            {this.listAccount()}
                        </select>
                        <br></br>

                        <div className="form-group">
                            <label >Số tài khoản người nhận</label>
                            <input type="text" defaultValue={this.state.defaultAccount} disabled={this.state.checkOTP} data-minlength="9" pattern="[0-9]+" maxLength="9" onChange={e => this.onAccountChange(e)} className="form-control" placeholder="Số tài khoản" required></input>
                            <div>
                                <span style={{ color: "gray" }}><small>Dãy số 9 kí tự</small></span>
                            </div>
                            {this.infoSpan('account')}
                        </div>

                        <div className="form-group">
                            <label >Tên người nhận</label>
                            <input type="text" value={this.state.name} disabled={true} className="form-control" placeholder="Tên người nhận" required></input>
                        </div>

                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label>Số tiền</label>
                                <input pattern="[0-9]+" maxLength="9" type="text" disabled={this.state.checkOTP} className="form-control" onChange={e => this.onAmountChange(e)} placeholder="Số tiền" required></input>
                                <div>
                                    <span style={{ color: "gray" }}><small>Tối thiểu 10.000 VND và tối đa 20.000.000 VND</small></span>
                                </div>
                                {this.infoSpan('amount')}
                            </div>
                            <div className="form-group col-md-6">
                                <label >Nội dung chuyển</label>
                                <input type="text" onChange={e => this.onDescriptionChange(e)} disabled={this.state.checkOTP} className="form-control" id="noidungchuyen" placeholder="Nội dung chuyển"></input>
                            </div>
                        </div>

                        <label>Hình thức thanh toán phí <span className="form-text">(phí chuyển tiền: 5.000 VND)</span></label>
                        <div>
                            <div className="custom-control custom-radio ">
                                <input onChange={this.onSenderFeerChange.bind(this)} type="radio" disabled={this.state.checkOTP} className="custom-control-input" name="feer" id="guitraphi" defaultChecked={true} value="sender"></input>
                                <label className="custom-control-label" htmlFor="guitraphi">Người gửi trả phí</label>
                            </div>
                            <div className="custom-control custom-radio">
                                <input  onChange={this.onReceiverFeerChange.bind(this)} type="radio" disabled={this.state.checkOTP} className="custom-control-input" name="feer" id="nhantraphi" value="receiver"></input>
                                <label className="custom-control-label" htmlFor="nhantraphi">Người nhận trả phí</label>
                            </div>
                        </div>
                        <br></br>
                        <label>Lưu người nhận</label>
                        <div className="custom-control custom-checkbox mb-3">
                            <input type="checkbox" className="custom-control-input " disabled={this.state.checkOTP} onChange={this.handleChecked} id="luunguoinhan" name="example1"></input>
                            <label className="custom-control-label" htmlFor="luunguoinhan">Lưu người nhận vào danh bạ</label>
                        </div>
                        <div className={!this.state.checkOTP ? 'hidden' : 'form-row'}>
                            <div className="form-group col-md-6">
                                <label>Xác nhận OTP: </label>
                                <input maxLength="10" type="text" onChange={this.onChangeOTP.bind(this)} className="form-control" placeholder="Hãy nhập mã OTP" required></input>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <Link to={`/dashboard`} activeclassname="active">
                                    <button type="submit" className="btn btn-huy float-right">Hủy</button>
                                </Link>
                            </div>
                            <div className="col-md-6">
                                <button onClick={this.sendMailOTP} className={this.state.checkOTP ? 'hidden' : 'btn btn-login float-left'}>Xác nhận</button>
                                <button type="submit" className={!this.state.checkOTP ? 'hidden' : 'btn btn-login float-left'}>Chuyển khoản</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div >
        );
    }
}

const mapStateToProps = state => {
    return {
        accounts: state.accountReducer.accounts
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchUser: (username) => {
            dispatch(fetchUser(username));
        },
        fetchAccount: (username) => {
            dispatch(fetchAccount(username));
        },
        showLoading: () => {
            dispatch(showLoading());
        },
        showError: (msg) => {
            dispatch(showError(msg));
        },
        showSuccess: (msg) => {
            dispatch(showSuccess(msg));
        },
        hideAllBox: () => {
            dispatch(hideAllBox());
        },
        fetchHistory: (username) => {
            dispatch(fetchHistory(username));
        },
        addContact: (username, account, alias) => {
            dispatch(addContact(username, account, alias))
        },
        fetchReceiver: (username) => {
            dispatch(fetchReceiver(username))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Transfermoney);
