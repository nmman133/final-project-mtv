import React, { Component } from 'react';
import { connect } from 'react-redux';
import bank from '../img/bank1.png';
import { showError, deleteAccount } from '../action/actionCreator';

class Card extends Component {
    constructor(props) {
        super(props);
        this.deleteAccount = this.deleteAccount.bind(this);
    }

    deleteAccount() {
        var username = sessionStorage.getItem('user');
        var account = this.props.info.account;
        var balance = this.props.info.balance;

        if (this.props.accounts.length < 2) {
            this.props.showError('Người dùng cần duy trì ít nhất một tài khoản');
            return;
        }

        if (balance > 5000) {
            this.props.showError('Không thể xóa tài khoản có số dư lớn hơn 5000 VND. Vui lòng chuyển toàn bộ số dư sang một tài khoản khác trước khi đóng');
            return;
        }

        this.props.deleteAccount(username, account);
    }

    render() {
        return (
            <div className="card">
                <img className="card-img-top" src={bank} alt=""></img>
                <div className="card-body">
                    <div className="row">
                        <h5 className="col card-title text-left"> ID: {this.props.info.account}</h5>
                        <button onClick={this.deleteAccount} className="btn btn-xoa-taikhoan ">Đóng tài khoản</button>
                    </div>
                    <h6 className="card-text text-left">Balance: {this.props.info.balance}</h6>

                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        accounts: state.accountReducer.accounts
    }
}

const mapDispatchToProps = dispatch => {
    return {
        deleteAccount: (username, account) => {
            dispatch(deleteAccount(username, account))
        },
        showError: (msg) => {
            dispatch(showError(msg))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Card);
