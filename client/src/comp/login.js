import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showError, showLoading, hideAllBox, updateUser } from '../action/actionCreator';
import '../css/login.css';
import Recaptcha from 'react-recaptcha';
import axios from 'axios';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            verified: false
        };
        this.submit = this.submit.bind(this);
        this.recaptchaVerify = this.recaptchaVerify.bind(this);
        this.resetCaptcha = this.resetCaptcha.bind(this);
    }

    submit(e) {
        e.preventDefault();
        var username = this.state.username.value;
        var password = this.state.password.value;
        const url = 'http://localhost:3300/login';
        this.props.showLoading();
        axios.post(url, {
            username: username,
            password: password
        }, { withCredentials: true }).then((res) => {
            if (res && res.data.auth === true) {
                sessionStorage.setItem('user', res.data.user.username);
                var emailInfo = {
                    fullname : res.data.user.fullname,
                    email : res.data.user.email
                }
                sessionStorage.setItem('emailInfo', JSON.stringify(emailInfo));
                this.props.hideAllBox();
                this.props.history.push('/dashboard');
            } else {
                this.props.showError(res.data.msg);
                this.resetCaptcha();
            }
        }).catch((e) => {
            this.props.showError('Không thể kết nối máy chủ!');
            this.resetCaptcha();
        });
    }

    resetCaptcha() {
        this.setState({
            verified: false
        });
        if (this.captcha) {
            this.captcha.reset();
        }
    }

    recaptchaVerify(token) {
        if (token.length >= 0) {
            this.setState({
                verified: true
            });
        }
    };

    render() {
        return (
            <div>
                <section className="container-fluid bg">
                    <br></br>
                    <br></br>
                    <p className="text-center"><strong>Chào mừng đến với MTVBank</strong>
                    </p>
                    <h4 className="text-center"><strong>Hãy đăng nhập để truy cập hệ thống</strong>
                    </h4>
                    <div className="row justify-content-center">
                        <form onSubmit={this.submit} className="col-3 rounded border shadow p-3 mb-5 bg-white centered" >
                            <div className="form-group">
                                <label>Tài khoản</label>
                                <input typr="text" id="user" className="form-control" placeholder="Tài khoản" ref={node => this.state.username = node} required></input>
                                <label>Mật khẩu</label>
                                <input type="password" id="pass" className="form-control" placeholder="Mật khẩu" ref={node => this.state.password = node} required></input>
                                <br></br>
                                <Recaptcha
                                    ref={e => (this.captcha = e)}
                                    sitekey="6LfSh4UUAAAAAH9VJAeGpFPz8_endNm1w3p5pJbi"
                                    verifyCallback={this.recaptchaVerify} />
                                <br></br>
                                <div className="text-center">
                                    <button type="submit" disabled={!this.state.verified} className="btn btn-login">Đăng nhập</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>);
    }
}

const mapStateToProps = state => {
    return {}
}

const mapDitpatchToProps = (dispatch) => {
    return {
        showError: (msg) => {
            dispatch(showError(msg));
        },
        showLoading: (msg) => {
            dispatch(showLoading());
        },
        hideAllBox: () => {
            dispatch(hideAllBox());
        },
        updateUser: (user) => {
            dispatch(updateUser(user));
        }
    }
}

export default connect(mapStateToProps, mapDitpatchToProps)(Login);
