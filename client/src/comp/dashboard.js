import React, { Component } from 'react';
import '../css/dashboard.css';
import { Link } from 'react-router-dom';

import Receiver from './receiver';
import History from './history';
import Header from './header';
import CardPanel from './cardpanel';

import { connect } from 'react-redux';
import { fetchAll } from '../action/actionCreator';

class Dashboard extends Component {
    componentDidMount() {
        this.props.fetchAll(sessionStorage.getItem('user'));
    }

    render() {
        return (
            <div>
                <Header />
                <br></br>
                <h3>THẺ CỦA BẠN
                    <Link to={`/transfermoney`} activeclassname="active">
                        <button type="submit" className="btn btn-add float-right"><i className="fa fa-cc-mastercard	"></i> Chuyển tiền</button>
                    </Link>
                </h3>
                <br></br>
                <CardPanel />
                <br></br>
                <h3>DANH SÁCH NGƯỜI NHẬN
                    <Link to={`/newcontact`} activeclassname="active">
                        <button type="submit" className="btn btn-add float-right"><i className="fa fa-plus-square"></i> Thêm</button>
                    </Link>
                </h3>
                <br></br>
                <div className="container-fluid align-middle col-md-10">
                    <Receiver />
                </div>
                <br></br>
                <h3>LỊCH SỬ GIAO DỊCH</h3>

                <br></br>
                <div className="container-fluid align-middle col-md-10">
                    <History />
                </div>
                <br></br><br></br>
                <footer>
                    <div className=" footer footer-bottom">
                        <div className="container">
                            <p className="pull-left"> Copyright © MTV 2019. All right reserved. </p>
                            <div className="pull-right">
                                <ul className="nav nav-pills payments">
                                    <li><i className="fa fa-cc-visa"></i></li>
                                    <li><i className="fa fa-cc-mastercard"></i></li>
                                    <li><i className="fa fa-cc-amex"></i></li>
                                    <li><i className="fa fa-cc-paypal"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        username: state.userReducer.user.username
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchAll: (username) => {
            dispatch(fetchAll(username));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
