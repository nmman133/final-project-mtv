import * as type from '../action/actionType';

const dummy = {
    receivers: []
}

export default function (state = dummy, action) {
    var newState = {...state};
    var payload = action.payload;
    switch (action.type) {
        case type.UPDATE_RECEIVER:
            newState.receivers = payload;
            break;
        default:
            break;
    }
    return newState;
}
