import * as type from '../action/actionType';

const dummy = {
    msgBox: {
        type: 0,
        msg: 'Nothing',
        enable: false
    }
}

export default function (state = dummy, action) {
    var newState = {...state};
    var payload = action.payload;
    switch (action.type) {
        case type.SHOW_SUCCESS_MSG_BOX:
            newState.msgBox = {
                type: 0,
                msg: payload,
                enable: true
            }
            break;
        case type.SHOW_ERROR_MSG_BOX:
            newState.msgBox = {
                type: -1,
                msg: payload,
                enable: true
            }
            break;
        case type.SHOW_LOADING_BOX:
            newState.msgBox.type = 1;
            newState.msgBox.enable = true;
            break;
        case type.HIDE_ALL_BOX:
            newState.msgBox.enable = false;
            break;
        default:
            break;
    }
    return newState;
}
