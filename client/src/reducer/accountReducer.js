import * as type from '../action/actionType';

const dummy = {
    accounts: []
}

export default function (state = dummy, action) {
    var newState = {...state};
    var payload = action.payload;
    switch (action.type) {
        case type.UPDATE_ACCOUNT:
            newState.accounts = payload;
            break;
        default:
            break;
    }
    return newState;
}
