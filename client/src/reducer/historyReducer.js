import * as type from '../action/actionType';

const dummy = {
    history: []
}
export default function (state = dummy, action) {
    var newState = {...state};
    var payload = action.payload;
    switch (action.type) {
        case type.UPDATE_HISTORY:
            newState.history = payload;
            break;
        default:
            break;
    }
    return newState;
}
