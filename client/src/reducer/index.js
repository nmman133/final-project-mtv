import { combineReducers } from 'redux';
import userReducer from './userReducer';
import accountReducer from './accountReducer';
import receiverReducer from './receiverReducer';
import historyReducer from './historyReducer';
import msgBoxReducer from './msgBoxReducer';

const rootReducer = combineReducers({
    userReducer,
    accountReducer,
    receiverReducer,
    historyReducer,
    msgBoxReducer
});

export default rootReducer;
