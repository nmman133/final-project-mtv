import * as type from './actionType';
import { callGraphqlApi } from './apiHelper';

export function updateUser(user) {
    return {
        type: type.UPDATE_USER,
        payload: user
    }
}

export function updateAccount(accounts) {
    return {
        type: type.UPDATE_ACCOUNT,
        payload: accounts
    }
}

export function updateReceiver(receivers) {
    return {
        type: type.UPDATE_RECEIVER,
        payload: receivers
    }
}

export function updateHistory(histories) {
    return {
        type: type.UPDATE_HISTORY,
        payload: histories
    }
}

export function showSuccess(msg) {
    return {
        type: type.SHOW_SUCCESS_MSG_BOX,
        payload: msg
    }
}

export function showError(msg) {
    return {
        type: type.SHOW_ERROR_MSG_BOX,
        payload: msg
    }
}

export function showLoading() {
    return {
        type: type.SHOW_LOADING_BOX,
        payload: null
    }
}

export function hideAllBox() {
    return {
        type: type.HIDE_ALL_BOX,
        payload: null
    }
}

export function fetchAll(username) {
    return dispatch => {
        var query = {
            "query": `{userByUsername(username: "${username}"){ \
                username fullname email \
                accounts{account type balance} \
                receivers{account alias} \
                transactions{source destination type amount transactionTime description}}}`
        };
        callGraphqlApi(query).then((res) => {
            if (res.status === 200 && !res.data.errors) {
                var data = res.data.data.userByUsername;
                dispatch(updateUser({
                    username: data.username,
                    fullname: data.fullname,
                    email: data.email
                }));
                dispatch(updateAccount(data.accounts));
                dispatch(updateReceiver(data.receivers));
                dispatch(updateHistory(data.transactions));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể nạp dữ liệu từ máy chủ. Vui lòng thử lại!'));
        });
    }
}

export function fetchUser(username) {
    return dispatch => {
        var query = {
            "query": `{userByUsername(username: "${username}"){username fullname email}}`
        };
        callGraphqlApi(query).then((res) => {
            if (res.status === 200 && !res.data.errors) {
                var data = res.data.data.userByUsername;
                dispatch(updateUser({
                    username: data.username,
                    fullname: data.fullname,
                    email: data.email
                }));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể nạp dữ liệu từ máy chủ. Vui lòng thử lại!'));
        });
    }
}

export function fetchAccount(username) {
    return dispatch => {
        var query = {
            "query": `{userByUsername(username: "${username}"){accounts{account type balance}}}`
        };
        callGraphqlApi(query).then((res) => {
            if (res.status === 200 && !res.data.errors) {
                var data = res.data.data.userByUsername;
                dispatch(updateAccount(data.accounts));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể nạp dữ liệu từ máy chủ. Vui lòng thử lại!'));
        });
    }
}

export function fetchReceiver(username) {
    return dispatch => {
        var query = {
            "query": `{userByUsername(username: "${username}"){receivers{account alias}}}`
        };
        callGraphqlApi(query).then((res) => {
            if (res.status === 200 && !res.data.errors) {
                var data = res.data.data.userByUsername;
                dispatch(updateReceiver(data.receivers));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể nạp dữ liệu từ máy chủ. Vui lòng thử lại!'));
        });
    }
}

export function fetchHistory(username) {
    return dispatch => {
        var query = {
            "query": `{userByUsername(username: "${username}"){ \
                transactions{source destination type amount transactionTime description}}}`
        };
        callGraphqlApi(query).then((res) => {
            if (res.status === 200 && !res.data.errors) {
                var data = res.data.data.userByUsername;
                dispatch(updateHistory(data.transactions));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể nạp dữ liệu từ máy chủ. Vui lòng thử lại!'));
        });
    }
}

export function addContact(username, account, alias) {
    return dispatch => {
        var query = {
            "query": `mutation{addReceiver(input: {username: "${username}", account: "${account}", alias: "${alias}"})}`
        };
        callGraphqlApi(query).then((res) => {
            if (res.status === 200 && !res.data.errors && res.data.data.addReceiver === true) {
                dispatch(showSuccess('Thêm tài khoản liên hệ thành công!'));
                dispatch(fetchReceiver(username));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể thêm tài khoản liên hệ. Vui lòng thử lại!'));
        });
    }
}

export function deleteContact(username, account) {
    return dispatch => {
        var query = {
            "query": `mutation{deleteReceiver(username: "${username}", account: "${account}")}`
        };
        callGraphqlApi(query).then((res) => {
            console.log(res);
            if (res.status === 200 && !res.data.errors && res.data.data.deleteReceiver === true) {
                dispatch(showSuccess('Xóa tài khoản liên hệ thành công!'));
                dispatch(fetchReceiver(username));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể xóa tài khoản liên hệ. Vui lòng thử lại!'));
        });
    }
}

export function deleteAccount(username, account) {
    return dispatch => {
        var query = {
            "query": `mutation{deleteAccount(account: "${account}")}`
        };
        callGraphqlApi(query).then((res) => {
            if (res.status === 200 && !res.data.errors && res.data.data.deleteAccount === true) {
                dispatch(showSuccess('Xóa tài khoản thành công!'));
                dispatch(fetchAccount(username));
            } else {
                throw new Error();
            }
        }).catch((e) => {
            console.log((e));
            dispatch(showError('Không thể xóa tài khoản. Vui lòng thử lại!'));
        });
    }
}
