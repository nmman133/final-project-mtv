import axios from 'axios';

export function callGraphqlApi(query) {
    const authUrl = 'http://localhost:3300/refresh';
    const graphqlUrl =  'http://localhost:3300/graphql';
    return axios.post(authUrl, null, {withCredentials: true}).then((res) => {
        if (res.data && res.data.auth === true) {
            return axios.post(graphqlUrl, query, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                },
                withCredentials: true
            });
        } else {
            throw new Error('refresh token fail');
        }
    });
}

export function sendMail(param) {
    const url = 'http://localhost:3300/sendMail';
    return axios.post(url, param, {
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        withCredentials: true
    });
}