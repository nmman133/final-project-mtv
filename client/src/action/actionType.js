// Server fetching
export const FETCH_ALL = 'FETCH_ALL';
export const FETCH_USER = 'FETCH_USER';
export const FETCH_ACCOUNT = 'FETCH_ACCOUNT';
export const FETCH_RECEIVER = 'FETCH_RECEIVER';
export const FETCH_HISTORY = 'FETCH_HISTORY';
export const TRANSFER = 'TRANSFER';
export const ADD_CONTACT = 'ADD_CONTACT';
export const DELETE_CONTACT = 'DELETE_CONTACT';

// Local update
export const UPDATE_USER = 'UPDATE_USER';
export const UPDATE_ACCOUNT = 'UPDATE_ACCOUNT';
export const UPDATE_RECEIVER = 'UPDATE_RECEIVER';
export const UPDATE_HISTORY = 'UPDATE_HISTORY';

// UI action
export const SHOW_SUCCESS_MSG_BOX = 'SHOW_SUCCESS_MSG_BOX';
export const SHOW_ERROR_MSG_BOX = 'SHOW_ERROR_MSG_BOX';
export const SHOW_LOADING_BOX = 'SHOW_LOADING_BOX';
export const HIDE_ALL_BOX = 'HIDE_ALL_BOX';
